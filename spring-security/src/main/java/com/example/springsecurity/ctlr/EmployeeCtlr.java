/**
 * 
 */
package com.example.springsecurity.ctlr;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springsecurity.entity.Employee;
import com.example.springsecurity.service.EmployeeService;

/**
 * The Class EmployeeCtlr.
 *
 * @author sony
 */
@RestController
public class EmployeeCtlr {

	/** The employee service. */
	@Autowired
	private EmployeeService employeeService;

	/**
	 * List all employees.
	 *
	 * @return the list
	 */
	@RequestMapping(value = "/listallemployees", method = RequestMethod.GET)
	public List<Employee> listAllEmployees(
			@org.springframework.security.core.annotation.AuthenticationPrincipal UserDetails userDetails) {
		String userName = userDetails.getUsername();
		String password = userDetails.getPassword();
		Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
		authorities.stream().forEach(System.out::println);
		return employeeService.listAllEmployees();
	}

	/**
	 * Find employee by id.
	 *
	 * @param id
	 *            the id
	 * @return the employee
	 */
	@RequestMapping(value = "/listemployee/{id}", method = RequestMethod.GET)
	public @ResponseBody Employee findEmployeeById(@PathVariable Long id) {
		return employeeService.findEmployeeById(id);
	}

	/**
	 * Delete employee by id.
	 *
	 * @param id
	 *            the id
	 * @return the string
	 */
	@RequestMapping(value = "/deleteemployee/{id}", method = RequestMethod.GET)
	public String deleteEmployeeById(@PathVariable Long id) {

		return employeeService.deleteEmployeeById(id);

	}

	/**
	 * Save employee.
	 *
	 * @param employee
	 *            the employee
	 * @return the string
	 */
	@RequestMapping(value = "/insertemployee", method = RequestMethod.POST)
	public String saveEmployee(@RequestBody Employee employee) {
		return employeeService.saveEmployee(employee);
	}

	/**
	 * Update employee by id.
	 *
	 * @param employee
	 *            the employee
	 * @param id
	 *            the id
	 * @return the string
	 */
	@RequestMapping(value = "/updateemployee", method = RequestMethod.POST)
	public String updateEmployeeById(@RequestBody Employee employee) {
		return employeeService.updateEmployeeById(employee);
	}

	/**
	 * Say hello employee.
	 *
	 * @return the string
	 */
	@RequestMapping(value = "/helloemployee", method = RequestMethod.GET)
	public String sayHelloEmployee() {
		return "Hello Employee";
	}

}
