/**
 * 
 */
package com.example.springsecurity.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springsecurity.entity.Employee;

/**
 * The Interface EmployeeDao.
 *
 * @author sony
 */
public interface EmployeeDao extends JpaRepository<Employee, Long> {

}
