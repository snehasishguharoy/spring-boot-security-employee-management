/**
 * 
 */
package com.example.springsecurity.service;

import java.util.List;

import com.example.springsecurity.entity.Employee;

/**
 * The Interface EmployeeService.
 *
 * @author sony
 */
public interface EmployeeService {

	/**
	 * List all employees.
	 *
	 * @return the list
	 */
	List<Employee> listAllEmployees();

	/**
	 * Find employee by id.
	 *
	 * @param id the id
	 * @return the employee
	 */
	Employee findEmployeeById(Long id);

	/**
	 * Delete employee by id.
	 *
	 * @param employee the employee
	 * @return the string
	 */
	String deleteEmployeeById(Long id);

	/**
	 * Save employee.
	 *
	 * @param employee the employee
	 * @return the string
	 */
	String saveEmployee(Employee employee);

	/**
	 * Update employee by id.
	 *
	 * @param employee the employee
	 * @param id the id
	 * @return the string
	 */
	String updateEmployeeById(Employee employee);

}
