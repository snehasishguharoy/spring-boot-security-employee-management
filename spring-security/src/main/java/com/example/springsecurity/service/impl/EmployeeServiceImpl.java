/**
 * 
 */
package com.example.springsecurity.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.springsecurity.dao.EmployeeDao;
import com.example.springsecurity.entity.Employee;
import com.example.springsecurity.service.EmployeeService;

/**
 * The Class EmployeeServiceImpl.
 *
 * @author sony
 */
@Component
public class EmployeeServiceImpl implements EmployeeService {

	/** The employee dao. */
	@Autowired
	private EmployeeDao employeeDao;

	/* (non-Javadoc)
	 * @see com.example.springsecurity.service.EmployeeService#listAllEmployees()
	 */
	@Override
	public List<Employee> listAllEmployees() {
		return employeeDao.findAll();
	}

	/* (non-Javadoc)
	 * @see com.example.springsecurity.service.EmployeeService#findEmployeeById(java.lang.Long)
	 */
	@Override
	public Employee findEmployeeById(Long id) {
		Employee employee= employeeDao.findOne(id);
		return employee;
	}

	/* (non-Javadoc)
	 * @see com.example.springsecurity.service.EmployeeService#deleteEmployeeById(com.example.springsecurity.entity.Employee)
	 */
	@Override
	public String deleteEmployeeById(Long id) {
		Employee employee=findEmployeeById(id);
		employeeDao.delete(employee);
		return "Successfully Deleted";
	}

	/* (non-Javadoc)
	 * @see com.example.springsecurity.service.EmployeeService#saveEmployee(com.example.springsecurity.entity.Employee)
	 */
	@Override
	public String saveEmployee(Employee employee) {
		employeeDao.save(employee);
		return "Saved Successfully";
	}

	/* (non-Javadoc)
	 * @see com.example.springsecurity.service.EmployeeService#updateEmployeeById(com.example.springsecurity.entity.Employee, java.lang.Long)
	 */
	@Override
	public String updateEmployeeById(Employee employee) {
		Employee emp = findEmployeeById(employee.getId());
		if (null != employee.getId()) {
			emp.setId(employee.getId());
		}
		if (null != employee.getEmpName()) {
			emp.setEmpName(employee.getEmpName());
		}
		if (null != employee.getDept()) {
			emp.setDept(employee.getDept());
		}
		employeeDao.save(emp);
		return "Updated Successfully";

	}

}
